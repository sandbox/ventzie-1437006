<?php

/**
 * @file
 * simple_comment_rate table installation. Also installing permissions
 * for every role.
 * Uninstalling module's variables on hook_uninstall().
 */

/**
 * Implements hook_schema().
 */
function simple_comment_rate_schema() {

  $schema['simple_comment_rate'] = array(
    'description' => 'Table where comment ratings will be recorded',
    'fields' => array(
      'cid' => array(
        'description' => 'Comment ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'ips' => array(
        'description' => 'IPs of the voters for a comment',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'uids' => array(
        'description' => 'User IDs of the voters for a comment',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'rateup' => array(
        'description' => 'Number of "LIKE" votes for a comment',
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'small',
        'not null' => TRUE,
      ),
      'ratedown' => array(
        'description' => 'Number of "DON`T LIKE" votes for a comment',
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'small',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('cid'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function simple_comment_rate_install() {
  $result = db_query("SELECT rid FROM {role}");

  foreach ($result as $row) {
    $rid = $row->rid;

    $query = db_insert('role_permission')
    ->fields(array(
              'rid' => $rid,
              'permission' => 'rate comment',
              'module' => 'simple_comment_rate',
            ))
    ->execute();

    $query = db_insert('role_permission')
    ->fields(array(
              'rid' => $rid,
              'permission' => 'view comment rates',
              'module' => 'simple_comment_rate',
            ))
    ->execute();
  }

  $types = node_type_get_types();
  $types_array = array();

  foreach ($types as $node_type) {
    $types_array[] = $node_type->type;
  }

  variable_set('scr_rate_this_content_types', $types_array);
}

/**
 * Implements hook_uninstall().
 */
function simple_comment_rate_uninstall() {
  variable_del('scr_rate_this_content_types');
  variable_del('scr_choose_how_to_vote');
  variable_del('scr_clear_cache');
}
