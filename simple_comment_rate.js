jQuery(document).ready(function () {

  jQuery('img.ratecommentcl').click(function () {

    var voteSaved = function (data) {
      jQuery('#rateupnumb_' + data.cid).text(data.votedup);
      jQuery('#ratedownnumb_' + data.cid).text(data.voteddown);
      var imgup = jQuery('#rate_up_' + data.cid).unbind('click');
      var imgdown = jQuery('#rate_down_' + data.cid).unbind('click');

      if(data.rate)
      {
        imgup.attr("src",data.picspath + "checkmark.png");
        imgdown.attr("src",data.picspath + "gray_down.png");
      }
      else
      {
        imgup.attr("src",data.picspath + "gray_up.png");
        imgdown.attr("src",data.picspath + "checkmark_red.png");
      }
    };

    jQuery.ajax({
      type: 'POST',
      url: get_simple_comment_rate_path(),
      dataType: 'json',
      success: voteSaved,
      data: "id=" + this.id + "&SCR_ajax=1&url=" + document.location.href + "&token=" + get_simple_comment_rate_token()
    });
  });
});
