<?php

/**
 * @file
 * Lets users rate comments.
 */

/**
 * Implements hook_init().
 */
function simple_comment_rate_init() {
  if (isset($_POST["SCR_ajax"]) || !user_access("view comment rates")) {
    return;
  }

  $token = drupal_get_token();

  drupal_add_js('function get_simple_comment_rate_token(){return "' . $token . '";}', 'inline');

  drupal_add_js(drupal_get_path('module', 'simple_comment_rate') . '/simple_comment_rate.js');

  $url = url('simple_comment_rate', array('absolute' => TRUE));
  drupal_add_js('function get_simple_comment_rate_path(){return "' . $url . '";}', 'inline');

  $path = isset($_GET['q']) ? $_GET['q'] : '<front>';
  $currenturl = url($path, array('absolute' => TRUE));
  drupal_add_js('function get_current_url(){return "' . $currenturl . '";}', 'inline');
}

/**
 * Implements hook_comment_view().
 */
function simple_comment_rate_comment_view($comment, $view_mode, $langcode) {

  if (!user_access("view comment rates")) {
    return;
  }

  $types = variable_get('scr_rate_this_content_types', array());
  $type = substr($comment->node_type, 13);

  if (!in_array($type, $types, TRUE)) {
    return;
  }

  global $user;

  $cid = $comment->cid;
  $ip = $user->hostname;

  $vote = simple_comment_rate_check_vote($cid, $ip, $user->uid);

  $comment->voted = $vote;

  list($rows_up_numb, $rows_down_numb) = simple_comment_rate_getcommentrate($cid);

  $comment->voted_up_numb = $rows_up_numb;
  $comment->voted_down_numb = $rows_down_numb;
}

/**
 * Implements hook_preprocess().
 */
function simple_comment_rate_preprocess_comment(&$variables) {
  $variables['title_prefix']['#theme'] = "simple_comment_rate";
  $variables['title_prefix']['comment'] = $variables['comment'];
}

/**
 * Implements hook_menu().
 */
function simple_comment_rate_menu() {
  $items['simple_comment_rate'] = array(
    'title' => 'Vote',
    'page callback' => 'simple_comment_rate_vote',
    'access callback' => 'user_access',
    'access arguments' => array('rate comment'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/simple_comment_rate'] = array(
    'title' => 'Comment rate',
    'description' => 'Adjust the rating mechanism',
    'position' => 'right',
    'weight' => -5,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/config/simple_comment_rate/settings'] = array(
    'title' => 'Simple comment rate settings',
    'description' => 'Adjust the rating mechanism',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('simple_comment_rate_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'simple_comment_rate.admin.inc',
  );

  return $items;
}

/**
 * Menu (/simple_comment_rate callback) function called by Ajax.
 */
function simple_comment_rate_vote() {
  global $user;

  $token = $_POST["token"];

  if (!drupal_valid_token($token)) {
    exit();
  }

  if ((variable_get('scr_choose_how_to_vote', 0) == 1) && $user->uid == 0) {
    exit();
  }

  $id = $_POST["id"];

  if (strpos($id, "rate_up_") !== FALSE) {
    $rate = 1;
    $cid = (int) substr($id, 8);
  }
  elseif (strpos($id, "rate_down_") !== FALSE) {
    $rate = 0;
    $cid = (int) substr($id, 10);
  }
  else {
    exit();
  }

  if ($cid <= 0) {
    exit();
  }

  $ip = $user->hostname;

  simple_comment_rate_updatetable($cid, $ip, $user->uid, $rate);

  if (variable_get('scr_clear_cache', 0) == 0) {
    simple_comment_rate_clearpagecache();
  }

  list($rows_up_numb, $rows_down_numb) = simple_comment_rate_getcommentrate($cid);

  drupal_json_output(array(
    'picspath' => simple_comment_rate_getpicspath(),
    'votedup' => $rows_up_numb,
    'voteddown' => $rows_down_numb,
    'rate' => $rate,
    'cid' => $cid,
  ));

  exit();
}

/**
 * Returns pics path.
 */
function simple_comment_rate_getpicspath() {
  global $base_url;
  $module_path = '/' . drupal_get_path('module', 'simple_comment_rate');
  $pics_path = $base_url . $module_path . "/pics/";

  return $pics_path;
}

/**
 * Check to see whether the user has voted already based on IP or user ID.
 */
function simple_comment_rate_check_vote($cid, $ip, $uid) {
  $vote = db_query("SELECT ips, uids FROM {simple_comment_rate} where cid = :cid", array(':cid' => $cid))->fetchAssoc();

  if ($vote === FALSE) {
    return FALSE;
  }

  $srch = (variable_get('scr_choose_how_to_vote', 0) == 0) ? "ips" : "uids";
  $srchval = (variable_get('scr_choose_how_to_vote', 0) == 0) ? $ip . ":" : $uid . ":";

  if (($i = strpos($vote[$srch], $srchval)) === FALSE) {
    return FALSE;
  }

  $res = (int) substr($vote[$srch], $i + strlen($srchval), 1);

  return $res;
}

/**
 * Returns UP and DOWN votes for a given comment ID.
 */
function simple_comment_rate_getcommentrate($cid) {
  $row = db_query("SELECT rateup, ratedown FROM {simple_comment_rate} where cid = :cid", array(':cid' => $cid))->fetchAssoc();

  if (!$row) {
    return array(0, 0);
  }

  return array($row["rateup"], $row["ratedown"]);
}

/**
 * Clears page cache for certain URL given by ajax.
 */
function simple_comment_rate_clearpagecache() {

  $url = $_POST['url'];

  if (!valid_url($url, TRUE)) {
    return;
  }

  global $base_url;

  $parsed = drupal_parse_url($url);

  $url = $parsed['path'];

  // This is done because of a bug in drupal_parse_url()
  $parts = explode("#", $url);
  $url = $parts[0];

  cache_clear_all($url, "cache_page", TRUE);
}

/**
 * Updates simple_comment_rate table when user votes on comment.
 */
function simple_comment_rate_updatetable($cid, $ip, $uid, $rate) {
  $row = db_query("SELECT * FROM {simple_comment_rate} where cid = :cid", array(':cid' => $cid))->fetchAssoc();

  if (!$row) {
    $res = db_insert('simple_comment_rate')
    ->fields(array(
      'cid' => $cid,
      'ips' => $ip . ":" . $rate . ";",
      'uids' => $uid . ":" . $rate . ";",
      'rateup' => ($rate) ? 1 : 0,
      'ratedown' => ($rate) ? 0 : 1,
    ))->execute();

    return;
  }

  $case = (variable_get('scr_choose_how_to_vote', 0) == 0) ? "ip" : "uid";

  if ($case == "ip") {
    if (strpos($row["ips"], $ip . ":") !== FALSE) {
      return;
    }
  }

  if ($case == "uid") {
    if (strpos($row["uids"], $uid . ":") !== FALSE) {
      return;
    }
  }

  $num_updated = db_update('simple_comment_rate')
  ->fields(array(
    'ips' => $row["ips"] . $ip . ":" . $rate . ";",
    'uids' => $row["uids"] . $uid . ":" . $rate . ";",
    'rateup' => ($rate) ? $row["rateup"] + 1 : $row["rateup"],
    'ratedown' => ($rate) ? $row["ratedown"] : $row["ratedown"] + 1,
  ))
  ->condition('cid', $cid)
  ->execute();
}

/**
 * Implements hook_permission().
 */
function simple_comment_rate_permission() {
  return array(
    'rate comment' => array(
      'title' => t('Rate comment'),
      'description' => t('Allow users to rate comments'),
    ),
    'view comment rates' => array(
      'title' => t('View comment rates'),
      'description' => t('Allow users to view comment rates'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function simple_comment_rate_theme() {
  return array(
    'simple_comment_rate' => array(
      'render element' => 'elements',
    ),
  );
}

/**
 * The theme function itself for rendering vote output.
 */
function theme_simple_comment_rate($variables) {
  if (!user_access("view comment rates")) {
    return "<!-- can not view comment rates -->";
  }

  $types = variable_get('scr_rate_this_content_types', array());
  $type = substr($variables['comment']->node_type, 13);

  if (!in_array($type, $types, TRUE)) {
    return "<!-- can not view comment rates -->";
  }

  global $user;

  $pics_path = simple_comment_rate_getpicspath();

  $vote = $variables['comment']->voted;

  $picup = $pics_path . (($vote === FALSE) ? 'up.png' : (($vote == 1) ? "checkmark.png" : "gray_up.png"));
  $picdown = $pics_path . (($vote === FALSE) ? 'down.png' : (($vote == 0) ? "checkmark_red.png" : "gray_down.png"));

  if ($vote === FALSE) {
    $rateclass = "class = 'ratecommentcl'";
  }
  else {
    $rateclass = "";
  }

  $cid = $variables['comment']->cid;

  $numbup = $variables['comment']->voted_up_numb;
  $numbuptext = "<span id=\"rateupnumb_" . $cid . "\" style=\"font-size:16px; color:#009933;\">$numbup</span>&nbsp;";

  $numbdown = $variables['comment']->voted_down_numb;
  $numbdowntext = "<span id=\"ratedownnumb_" . $cid . "\" style=\"font-size:16px; color:#990033;\">$numbdown</span>&nbsp;";

  $idup = "id = 'rate_up_" . $cid . "'";
  $iddown = "id = 'rate_down_" . $cid . "'";

  $style = "style=\"cursor:pointer;\"";

  $t = get_t();
  $like = $t('Like');
  $dislike = $t('Dislike');

  $title_attribute_like = "title = '$like'";
  $title_attribute_disike = "title = '$dislike'";

  $zoom = "onmouseover='this.width=this.width*1.3' onmouseout='this.width=this.width/1.2' " . $style;

  if (!user_access("rate comment")) {
    $title_attribute_like = 'title = "' . $t("You don't have permissions to vote") . '"';
    $title_attribute_disike = 'title = "' . $t("You don't have permissions to vote") . '"';

    $picup = $pics_path . "gray_up.png";
    $picdown = $pics_path . "gray_down.png";

    $rateclass = "";
  }
  elseif ((variable_get('scr_choose_how_to_vote', 0) == 1) && $user->uid == 0) {
    $title_attribute_like = "title = '" . $t('Log in to vote') . "'";
    $title_attribute_disike = "title = '" . $t('Log in to vote') . "'";

    $picup = $pics_path . "gray_up.png";
    $picdown = $pics_path . "gray_down.png";

    $rateclass = "";
  }

  $output = "<div style=\"float:right;\">
  <img $rateclass $idup $title_attribute_like $zoom src='$picup'>" . $numbuptext . "
  <img $rateclass $iddown $title_attribute_disike $zoom src='$picdown'>" . $numbdowntext . "</div>";

  return $output;
}

/**
 * Move comment object for easier access.
 */
function template_preprocess_simple_comment_rate(&$variables) {
  $variables['comment'] = $variables['elements']['comment'];
}

/**
 * Implements hook_form_alter().
 */
function simple_comment_rate_form_comment_form_alter(&$form, &$form_state, $form_id) {

  if (!isset($form['cid']['#value'])) {
    return;
  }

  $cid = $form['cid']['#value'];

  global $user;

  $types = node_type_get_types();
  $comment_form_types = array();

  foreach ($types as $node_type) {
    $comment_form_types[] = "comment_node_{$node_type->type}_form";
  }

  if (in_array($form_id, $comment_form_types) && in_array('administrator', array_values($user->roles))) {
    list($up, $down) = simple_comment_rate_getcommentrate($cid);

    $form['rate'] = array(
      '#type' => 'fieldset',
      '#title' => t('Rate comments'),
      '#collapsible' => TRUE,
    );

    $form['rate']['votes'] = array(
      '#markup' => "<br/><strong>RATE:</strong> up - $up ; down - $down",
    );

    $form['rate']['clear_rate'] = array(
      '#type' => 'checkbox',
      '#title' => t('Clear the rate for this comment'),
      '#default_value' => 0,
    );

    $form['#submit'][] = 'simple_comment_rate_clear_rate';
  }
}

/**
 * Submit function that clears vote for a comment.
 */
function simple_comment_rate_clear_rate($form, &$form_state) {

  if ($form_state['values']['clear_rate'] > 0) {
    $cid = $form['cid']['#value'];

    db_delete('simple_comment_rate')
    ->condition('cid', $cid)
    ->execute();
  }
}

/**
 * Implements hook_comment_delete().
 */
function simple_comment_rate_comment_delete($comment) {
  db_delete('simple_comment_rate')
    ->condition('cid', $comment->cid)
    ->execute();
}

/**
 * Implements hook_help().
 */
function simple_comment_rate_help($path, $arg) {
  switch ($path) {
    case "admin/help#Simple_Comment_Rate":
      $output = '<h3>' . t("About") . '</h3><p>';
      $output .= '<p>' . t("Allows users to rate comments") . '</p>';
      return $output;
      break;
  }
}
