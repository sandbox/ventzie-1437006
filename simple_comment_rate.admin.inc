<?php

/**
 * @file
 * Administration page callbacks for the simple_comment_rate module.
 */

/**
 * The drupal_get_form() argument callback function.
 */
function simple_comment_rate_admin_settings() {
  $form['scr_choose_how_to_vote'] = array(
    '#type' => 'radios',
    '#title' => t('Users will be restricted to vote more than once for a certain comment'),
    '#description' => t('Select a method for restricting users to vote second time for a comment'),
    '#options' => array(
      t('By IP.'),
      t('By user ID (only logged in users can vote).'),
    ),
    '#default_value' => variable_get('scr_choose_how_to_vote', 0),
  );

  $form['scr_clear_cache'] = array(
    '#type' => 'radios',
    '#title' => t('Choose whether to clear page cache where comment resides.'),
    '#description' => t("When anonymous users are served cached pages they won't be able to see actual vote until cache expires if it is not cleared."),
    '#options' => array(
      t('Clear page cache after vote (Recommended).'),
      t('Do not clear page cache after vote.'),
    ),
    '#default_value' => variable_get('scr_clear_cache', 0),
  );

  $types = node_type_get_types();
  $types_array = array();

  foreach ($types as $node_type) {
    $options[$node_type->type] = $node_type->name;
    $types_array[] = $node_type->type;
  }

  $form['scr_rate_this_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types that can be rated'),
    '#options' => $options,
    '#default_value' => variable_get('scr_rate_this_content_types', $types_array),
    '#description' => t('Users may rate comments on these content types.'),
  );

  $form['#submit'][] = 'simple_comment_rate_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Page cache needs to be cleared after user changes some settings.
 */
function simple_comment_rate_admin_settings_submit() {
  cache_clear_all("*", "cache_page", TRUE);
}
