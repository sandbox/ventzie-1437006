This module is created for the users to be able to rate comments.
Users have the option to rate a comment either once from a certain IP
or once with certain user ID. Administrators can choose which content
type comments can be rated.

Installation
------------

Copy folder to sites/all/modules directory and then enable on the admin
modules page. Go to settings to configure the module

Author
------
Ventzislav Mitev
ventzie@yahoo.com
